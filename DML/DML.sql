--DML BASE DE DATOS
USE CiberZ1373

EXECUTE SP_AgregarComputadoras 'N/A', '0.0.0.0', 0
EXECUTE SP_AgregarComputadoras 'PC-1', '192.168.1.2', 0
EXECUTE SP_AgregarComputadoras 'PC-2', '192.168.1.3', 0
EXECUTE SP_AgregarComputadoras 'PC-3', '192.168.1.4', 0
EXECUTE SP_AgregarComputadoras 'PC-4', '192.168.1.5', 0
EXECUTE SP_AgregarComputadoras 'PC-5', '192.168.1.6', 0
EXECUTE SP_AgregarComputadoras 'PC-6', '192.168.1.7', 0
EXECUTE SP_AgregarComputadoras 'PC-7', '192.168.1.8', 0
EXECUTE SP_AgregarComputadoras 'PC-8', '192.168.1.9', 0
EXECUTE SP_AgregarComputadoras 'PC-9', '192.168.1.10', 0
EXECUTE SP_AgregarComputadoras 'PC-10', '192.168.1.11', 0

EXECUTE SP_AgregarUsuarios 'admin', 'admin'
EXECUTE SP_AgregarUsuarios 'c-ortiz', 'admin'
EXECUTE SP_AgregarUsuarios 'e-cerme�o', 'admin'
EXECUTE SP_AgregarUsuarios 'j-morente', 'admin'
EXECUTE SP_AgregarUsuarios 'j-castellanos', 'admin'
EXECUTE SP_AgregarUsuarios 'l-velasquez', 'admin'

EXECUTE SP_AgregarClientes 'Consumidor Final', 'CF'
EXECUTE SP_AgregarClientes 'Daniel Gonzalez', '123456-1'
EXECUTE SP_AgregarClientes 'Rafael Gonzalez', '98765-2'
EXECUTE SP_AgregarClientes 'Fernando Zacarias', '5467354-3'
EXECUTE SP_AgregarClientes 'Xavier Perez', '2345987-4'
EXECUTE SP_AgregarClientes 'Rene Ramos', '3434709-5'

EXECUTE SP_AgregarSocios 'N/A', 'N/A', 'N/A', 'No Aplica', 0, '1/1/0001', 0, 'N/A', 0
EXECUTE SP_AgregarSocios 'admin', 'admin', 'administrador', 'CyberZ1373', 19287346, '04/12/1999', 56473623, 'Zona 10', 100

EXECUTE SP_AgregarTipo 'No Aplica'
EXECUTE SP_AgregarTipo 'Alquiler de Equipo'
EXECUTE SP_AgregarTipo 'Escaneos'
EXECUTE SP_AgregarTipo 'Impresiones'
EXECUTE SP_AgregarTipo 'Grabaciones CD/DVD/BlueRay'
EXECUTE SP_AgregarTipo 'Insumos de Computaci�n'
EXECUTE SP_AgregarTipo 'Cafeteria'


EXECUTE SP_AgregarArticulos 'No Aplica', 'NOAPLICA', 1, 0.00, 'N/A' 
EXECUTE SP_AgregarArticulos 'Coca Cola', 'HFH1231', 7, 8.00, '25'
EXECUTE SP_AgregarArticulos 'M&Ms', 'MHJKJ098', 7, 7.00, '100'
EXECUTE SP_AgregarArticulos 'Skittles', 'SKT678', 7, 7.00, '100'
EXECUTE SP_AgregarArticulos 'Impresion Negro', 'IMPN123', 4, 1.00, 'N/A'
EXECUTE SP_AgregarArticulos 'Impresion Color', 'IMPC123', 4, 2.00, 'N/A'
EXECUTE SP_AgregarArticulos 'DVD', 'DVD123', 5, 8.00, '150'
EXECUTE SP_AgregarArticulos 'CD', 'CD123', 5, 6.00, '150'

EXECUTE SP_AgregarHoras '00:00', 0
EXECUTE SP_AgregarHoras '01:00', 5
EXECUTE SP_AgregarHoras '00:30', 3
EXECUTE SP_AgregarHoras '00:15', 2