--DDL BASE DE DATOS
--CREAR BASE DE DATOS CIBER Z1373

CREATE DATABASE CiberZ1373
GO

--USAR BASE DE DATOS CIBER Z1373
USE CiberZ1373
GO

--CREAR TABLA USUARIOS
CREATE TABLE Usuarios(
	idUsuario INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	nombre VARCHAR(25) NOT NULL,
	clave VARCHAR(100) NOT NULL
);

--CREAR TABLA COMPUTADORAS
CREATE TABLE Computadoras(
	idComputadora INT IDENTITY (1,1) NOT NULL PRIMARY KEY,
	nombre VARCHAR(25) NOT NULL,
	ip VARCHAR(255) NOT NULL,
	activo BIT NOT NULL DEFAULT 0
);

--CREAR TABLA HORAS
CREATE TABLE Horas(
	idHora INT IDENTITY (1,1) NOT NULL PRIMARY KEY,
	tiempo TIME NULL,
	precio MONEY NOT NULL
);

--CREAR TABLA TIPO ARTICULO
CREATE TABLE Tipo(
	idTipo INT IDENTITY (1,1) NOT NULL PRIMARY KEY,
	nombre VARCHAR(255) NOT NULL
);

--CREAR TABLA ARTICULO
CREATE TABLE Articulos(
	idArticulo INT IDENTITY (1,1) NOT NULL PRIMARY KEY,
	nombre VARCHAR(100) NOT NULL,
	codigo VARCHAR(100) NOT NULL,
	idTipo INT NOT NULL,
	precio MONEY NOT NULL,
	cantidad VARCHAR(20) NOT NULL,
	FOREIGN KEY (idTipo) REFERENCES Tipo (idTipo)
	ON UPDATE CASCADE
	ON DELETE CASCADE
);

--CREAR TABLA CUPONES
CREATE TABLE Cupones(
	idCupones INT IDENTITY (1,1) NOT NULL PRIMARY KEY,	
	usuario VARCHAR (25) NOT NULL,
	clave VARCHAR (25) NOT NULL,
	estado BIT DEFAULT 1
);
GO

--CREAR TABLA SOCIOS
CREATE TABLE Socios(
	idSocio INT IDENTITY (1,1) NOT NULL PRIMARY KEY,	
	usuario VARCHAR (25) NOT NULL,
	clave VARCHAR (25) NOT NULL,
	nombre VARCHAR (25) NOT NULL,
	apellido VARCHAR (25) NOT NULL,
	dpi INT NOT NULL,
	fechadeNacimiento DATE NOT NULL,
	telefono INT NOT NULL,
	direccion VARCHAR (50) NOT NULL,
	saldoInicial INT NOT NULL	
);

--CREAR CLIENTES
CREATE TABLE Clientes(
	idCliente INT IDENTITY (1,1) NOT NULL PRIMARY KEY,
	nombre VARCHAR (100) NOT NULL,
	nit VARCHAR (100) NULL
);

--CREAR TABLA FACTURAS
CREATE TABLE Factura(
	idFactura INT IDENTITY (1,1) NOT NULL PRIMARY KEY,
	fecha DATETIME NULL DEFAULT CURRENT_TIMESTAMP,	
	idSocio INT NULL,
	idCliente INT NULL,	
	total MONEY NOT NULL DEFAULT 0,
	iva MONEY NOT NULL DEFAULT 0,	
	FOREIGN KEY (idSocio) REFERENCES Socios (idSocio)
	ON UPDATE CASCADE
	ON DELETE CASCADE,	
	FOREIGN KEY (idCliente) REFERENCES Clientes (idCliente)
	ON UPDATE CASCADE
	ON DELETE CASCADE
);

--CREAR TABLA DETALLE FACTURA
CREATE TABLE DetalleFactura(
	idDetalleFactura INT IDENTITY (1,1) NOT NULL PRIMARY KEY,
	idFactura INT NOT NULL,
	idComputadora INT NULL,
	idHora INT NULL,
	idArticulo INT NULL,
	FOREIGN KEY (idComputadora) REFERENCES Computadoras (idComputadora)
	ON UPDATE CASCADE
	ON DELETE CASCADE,
	FOREIGN KEY (idArticulo) REFERENCES Articulos (idArticulo)
	ON UPDATE CASCADE
	ON DELETE CASCADE,
	FOREIGN KEY (idFactura) REFERENCES Factura (idFactura)
	ON UPDATE CASCADE
	ON DELETE CASCADE,
	FOREIGN KEY (idHora) REFERENCES Horas (idHora)
	ON UPDATE CASCADE
	ON DELETE CASCADE
);
GO

--PROCEDIMIENTOS ALMACENADOS
--PROCEDIMIENTO ALMACENADO DE USUARIO
CREATE PROCEDURE SP_AgregarUsuarios
	@Usuario VARCHAR(255), @Clave VARCHAR(255)
	AS
	BEGIN
		INSERT INTO Usuarios(nombre, clave)
			VALUES(@Usuario, @Clave)
	END
GO

--PROCEDIMIENTO ALMACENADO DE COMPUTADORAS
CREATE PROCEDURE SP_AgregarComputadoras
	@Nombre VARCHAR(255), @Ip VARCHAR(255), @Activo BIT
	AS
	BEGIN
		INSERT INTO Computadoras(nombre, ip, activo)
			VALUES(@Nombre, @Ip, @Activo)
	END
GO

--PROCEDIMIENTO ALMACENADO DE TIPOS
CREATE PROCEDURE SP_AgregarTipo
	@Nombre VARCHAR(255)
	AS
	BEGIN
	INSERT INTO Tipo(nombre)
			VALUES(@Nombre)
	END
GO

--PROCEDIMIENTO ALMACENADO DE ARTICULOS
CREATE PROCEDURE SP_AgregarArticulos
	@Nombre VARCHAR(255), @Codigo VARCHAR(255), @IdTipo INTEGER,
		@Precio DECIMAL(5,2), @Cantidad VARCHAR(255)
	AS
	BEGIN
	INSERT INTO Articulos(nombre, codigo, idTipo, precio, cantidad)
		VALUES(@Nombre, @Codigo, @IdTipo, @Precio, @Cantidad)
	END
GO

--PROCEDIMIENTO ALMACENADO DE SOCIOS
CREATE PROCEDURE SP_AgregarSocios
	@Usuario VARCHAR(255), @Clave VARCHAR(255), @Nombre VARCHAR(255), @Apellido VARCHAR(255),
		@Dpi INTEGER, @FechaNacimiento Date, @Telefono INTEGER, @Direccion VARCHAR(255),
			@SaldoInicial INTEGER
	AS
	BEGIN
	INSERT INTO Socios(usuario, clave, nombre, apellido, dpi, fechadeNacimiento,
		telefono, direccion, saldoInicial)
			VALUES(@Usuario, @Clave, @Nombre, @Apellido, @Dpi, @FechaNacimiento,
				@Telefono, @Direccion, @SaldoInicial)
	END
GO

--PROCEDIMIENTO ALMACENADO DE CLIENTES
CREATE PROCEDURE SP_AgregarClientes
	@Nombre VARCHAR(255), @Nit VARCHAR(255)
	AS
	BEGIN
	INSERT INTO Clientes(nombre, nit)
		VALUES(@Nombre, @Nit)
	END
GO

--PROCEDIMIENTO ALMACENADO DE HORAS
CREATE PROCEDURE SP_AgregarHoras
	@Tiempo TIME, @Precio MONEY
	AS
	BEGIN
	INSERT INTO Horas(tiempo, precio)
		VALUES(@Tiempo, @Precio)
	END
GO

--PROCEDIMIENTO ALMACENADO PARA MOSTRAR FACTURAS
CREATE PROCEDURE SP_MostrarFactura
	@idFactura INTEGER
	AS
	BEGIN
	SELECT DetalleFactura.idDetalleFactura AS 'ID',
	Factura.idFactura AS 'No. Fac.', Socios.nombre AS 'Socio',
	Clientes.nombre AS 'Cliente', 
	Computadoras.nombre AS 'Computadoras Alquiladas',
	Horas.tiempo AS 'Tiempo',FORMAT(Horas.precio, 'C', 'es-GT') AS 'Horas Alquiladas', Articulos.nombre AS 'Articulos',
	FORMAT(Articulos.precio, 'C', 'es-GT') AS 'Precio Articulo', FORMAT(Factura.total, 'C', 'es-GT') AS 'Total', 
	FORMAT(Factura.iva, 'C', 'es-GT') AS 'IVA'  FROM DetalleFactura 
		INNER JOIN Computadoras ON DetalleFactura.idComputadora = Computadoras.idComputadora
		INNER JOIN Horas ON DetalleFactura.idHora = Horas.idHora
		INNER JOIN Factura ON DetalleFactura.idFactura = Factura.idFactura 
		INNER JOIN Articulos ON DetalleFactura.idArticulo = Articulos.idArticulo
		INNER JOIN Socios ON Factura.idSocio = Socios.idSocio
		INNER JOIN Clientes ON Factura.idCliente = Clientes.idCliente
		WHERE DetalleFactura.idFactura = @idFactura
	END
GO

CREATE VIEW VIEW_Facturas
AS
SELECT DetalleFactura.idDetalleFactura AS 'ID',
	Factura.idFactura AS 'No. Fac.', Socios.nombre AS 'Socio',
	Clientes.nombre AS 'Cliente', 
	Computadoras.nombre AS 'Computadoras Alquiladas',
	Horas.tiempo AS 'Tiempo',FORMAT(Horas.precio, 'C', 'es-GT') AS 'Horas Alquiladas', Articulos.nombre AS 'Articulos',
	FORMAT(Articulos.precio, 'C', 'es-GT') AS 'Precio Articulo', FORMAT(Factura.total, 'C', 'es-GT') AS 'Total', 
	FORMAT(Factura.iva, 'C', 'es-GT') AS 'IVA'  FROM DetalleFactura 
		INNER JOIN Computadoras ON DetalleFactura.idComputadora = Computadoras.idComputadora
		INNER JOIN Horas ON DetalleFactura.idHora = Horas.idHora
		INNER JOIN Factura ON DetalleFactura.idFactura = Factura.idFactura 
		INNER JOIN Articulos ON DetalleFactura.idArticulo = Articulos.idArticulo
		INNER JOIN Socios ON Factura.idSocio = Socios.idSocio
		INNER JOIN Clientes ON Factura.idCliente = Clientes.idCliente
GO